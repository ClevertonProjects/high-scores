using UnityEngine;

/*! \class HighScoreManager
 *  \brief Manages the saved high scores.
 *  
 *  Adds new score values in the score table. Can be used with the save system library to create external data files.
 */ 
public class HighScoreManager 
{
    public const int TABLE_SIZE = 5;                                                     //!< Amount of scores saved in the score table.
    private Score[] highScores = new Score[TABLE_SIZE];                                  //!< High scores data.    

    /// Finds a score rank position.     
    private int GetScoreRank (int score)
    {        
        int i = TABLE_SIZE - 1;        

        while (i >= 0 && highScores[i].ScoreValue < score)
        {
            i--;            
        }

        return i;        
    }

    /// <summary>
    /// Checks if a new score is higher than the last rank position to enter in the highscore table.
    /// </summary> 
    public bool IsHighScore (int score)
    {     
        return score > highScores[TABLE_SIZE - 1].ScoreValue;
    }

    /// <summary>
    /// Returns the high scores data.
    /// </summary>    
    public Score[] GetHighScores () 
    {
        return highScores;
    }

    /// <summary>
    /// Creates an initial score table with custom values.
    /// </summary>    
    public void CreateInitialTable (int higherInitialScore = 100)
    {
        int letter01;
        int letter02;
        int letter03;
        string randomName;

        for (int i = 0; i < TABLE_SIZE; i++)
        {
            letter01 = Random.Range(65, 91);
            letter02 = Random.Range(65, 91);
            letter03 = Random.Range(65, 91);
            randomName = char.ConvertFromUtf32(letter01) + char.ConvertFromUtf32(letter02) + char.ConvertFromUtf32(letter03);

            highScores[i].EntryName = randomName;
            highScores[i].ScoreValue = higherInitialScore / (i + 1);
        }                
    }

    /// <summary>
    /// Loads the saved high scores, optionally creates an initial table.
    /// </summary>    
    public void FillScoreTable (Score[] scoreTable)
    {
        highScores = scoreTable;
    }

    /// <summary>
    /// Receives a new score data.
    /// </summary>    
    public void NewScore (string newName, int newScore)
    {
        if (IsHighScore(newScore))
        {
            int scoreRank = GetScoreRank(newScore);  // Finds the new score position rank.

            // If the value is not in the last rank position, the array need to be rearranged.
            if (scoreRank < TABLE_SIZE - 1)
            {                
                for (int i = TABLE_SIZE - 2; i >= scoreRank; i--)
                {
                    highScores[i + 1].EntryName = highScores[i].EntryName;
                    highScores[i + 1].ScoreValue = highScores[i].ScoreValue;
                }
            }            

            highScores[scoreRank].EntryName = newName;
            highScores[scoreRank].ScoreValue = newScore;            
        }
    }
}
