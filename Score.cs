﻿/*! \struct Score
 *  \brief Data saved on high score table. 
 */
[System.Serializable]
public struct Score
{
    public string EntryName;                        //!< Score owner name.
    public int ScoreValue;                          //!< Score value.
}